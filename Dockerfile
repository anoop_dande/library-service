FROM java:8
EXPOSE 8080
ADD /target/LibraryServices-0.0.1-SNAPSHOT.jar LibraryServices-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java", "-jar", "LibraryServices-0.0.1-SNAPSHOT.jar"]