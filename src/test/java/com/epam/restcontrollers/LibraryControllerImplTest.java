//package com.epam.restcontrollers;
//
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.junit.jupiter.api.Assertions.fail;
//import static org.mockito.Mockito.when;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.springframework.http.ResponseEntity;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//
//import com.epam.dto.BookDTO;
//import com.epam.dto.UserDTO;
//import com.epam.dto.UserDetailsWithIssuedBooksDTO;
//import com.epam.restcontrollers.v1.LibraryControllerImpl;
//import com.epam.services.LibraryService;
//import com.fasterxml.jackson.databind.ObjectMapper;
//
//class LibraryControllerImplTest {
//	
//	
//	private MockMvc mockMvc;
//
//	@Mock
//	LibraryService libraryServices;
//
//	@InjectMocks
//	LibraryControllerImpl libraryController;
//
//	@BeforeEach
//	public void setup() {
//		MockitoAnnotations.initMocks(this);
//		this.mockMvc = MockMvcBuilders.standaloneSetup(libraryController).build();
//	}
//
//	static UserDTO userDTO;
//	static BookDTO book;
//	static ArrayList users;
//	static List<BookDTO> bookList;
//
//
//	public static String asJsonString(final Object obj) {
//		try {
//			return new ObjectMapper().writeValueAsString(obj);
//		} catch (Exception e) {
//			throw new RuntimeException(e);
//		}
//	}
//
//	@BeforeAll
//	public static void initVars() {
//		userDTO = new UserDTO(1L, "Anoop", "Dande", "anoop_dande@epam.com");
//		book=new BookDTO(1L, "java", "james");
//		users = new ArrayList();
//		((List<UserDTO>) users).add(userDTO);
//		bookList = new ArrayList<BookDTO>();
//		bookList.add(new BookDTO());
//	}
//
//	@Test
//	void testGetAllUsers() throws Exception {
//		this.mockMvc.perform(get("/library/users")).andExpect(status().isOk());
//	}
//
//	@Test
//	void testGetUserById() throws Exception {
//		this.mockMvc.perform(get("/library/users/{userId}", userDTO.getId())).andExpect(status().isOk());
//	}
//
//	@Test
//	void testDeleteById() throws Exception {
//		this.mockMvc.perform(delete("/library/users/{userId}", userDTO.getId())).andExpect(status().is2xxSuccessful());
//	}
//
//	@Test
//	void testUpdateById() throws Exception {
//		this.mockMvc.perform(put("/library/users/{userId}", userDTO.getId()).contentType("application/json")
//				.content(asJsonString(userDTO))).andExpect(status().isOk());
//	}
//
//	@Test
//	void testAddUser() throws Exception {
//
//		this.mockMvc.perform(post("/library/users").contentType("application/json").content(asJsonString(userDTO)))
//				.andExpect(status().isOk());
//	}
//
//
//	@Test
//	void testAddBook() throws Exception {
//		this.mockMvc.perform(post("/library/books").contentType("application/json").content(asJsonString(book)))
//		.andExpect(status().isOk());
//	}
//
//	@Test
//	void testGetAllBooks() throws Exception {
//		this.mockMvc.perform(get("/library/books")).andExpect(status().isOk());
//
//	}
//
//	@Test
//	void testUpdateLongBook() throws Exception {
//		this.mockMvc.perform(put("/library/books/{bookId}", book.getId()).contentType("application/json")
//				.content(asJsonString(book))).andExpect(status().isOk());
//	}
//
//	@Test
//	void testGetBookById() throws Exception {
//		this.mockMvc.perform(get("/library/books/{bookId}", book.getId())).andExpect(status().isOk());
//	}
//
//	@Test
//	void testDeleteBook() throws Exception {
//		this.mockMvc.perform(delete("/library/books/{bookId}", book.getId())).andExpect(status().is2xxSuccessful());
//
//	}
//	
//	@Test
//	void testIssueBookToUser() throws Exception {
//		this.mockMvc.perform(post("/library/users/{user_id}/books/{book_id}", userDTO.getId(),book.getId())).andExpect(status().is2xxSuccessful());
//
//	}
//	
//	@Test
//	void testReleaseBookFromUser() throws Exception {
//		this.mockMvc.perform(delete("/library/users/{user_id}/books/{book_id}", userDTO.getId(),book.getId())).andExpect(status().is2xxSuccessful());
//
//	}
//}
