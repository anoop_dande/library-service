//package com.epam.repository;
//
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.fail;
//import static org.mockito.Mockito.when;
//
//import java.util.List;
//import java.util.Optional;
//
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
//import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
//import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
//
//import com.epam.entity.IssuedBook;
//
//@DataJpaTest
//@AutoConfigureTestDatabase(replace = Replace.NONE)
//class LibraryRepositoryTest {
//	
//	@Autowired
//	LibraryRepository libraryRepository;
//
//	static IssuedBook issuedBook;
//	
//	@Mock
//	LibraryRepository libraryRepository1;
//
//
//	@BeforeEach
//	void init() {
//		MockitoAnnotations.initMocks(this);
//	}
//
//	@BeforeAll
//	public static void initVars() {
//		issuedBook = new IssuedBook();
//		issuedBook.setId(1L);
//		issuedBook.setUserId(1L);
//		issuedBook.setBookId(1L);
//	}
//	
//	@Test
//	public void testSave() {
//		libraryRepository.save(issuedBook);
//		assertEquals(1, issuedBook.getBookId());
//	}
//
//	@Test
//	void testFindAll() {
//		libraryRepository.save(issuedBook);
//		List<IssuedBook> issuedBookList = libraryRepository.findAll();
//		assertThat(issuedBookList).isNotEmpty();
//	}
//
//
//}
