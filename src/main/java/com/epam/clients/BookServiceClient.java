package com.epam.clients;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.epam.dto.BookDTO;


/**
 * @author Anoop_Dande
 *
 *Feign client book service
 */
@FeignClient(name = "book-service",fallback = BookClientFallback.class)
@Component
public interface BookServiceClient {

	@PostMapping("/books")
	public ResponseEntity<BookDTO> addBook(@RequestBody BookDTO book);

	@GetMapping("/books")
	public ResponseEntity<List<BookDTO>> getAllBooks();

	@PutMapping("/books/{bookId}")
	public ResponseEntity<BookDTO> update(@PathVariable Long bookId, @RequestBody BookDTO book);

	@GetMapping("/books/{bookId}")
	public ResponseEntity<BookDTO> getBookById(@PathVariable Long bookId);

	@DeleteMapping("/books/{bookId}")
	public ResponseEntity<?> deleteBook(@PathVariable Long bookId);

}
