package com.epam.clients;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.epam.dto.UserDTO;
import com.epam.utils.ResponseEntitiesUtil;

/**
 * @author Anoop_Dande
 *
 *Fallback for the userServiceClient
 */
@Component
public class UserClientFallback implements UserServiceClient{

	@Override
	public ResponseEntity<UserDTO> addUserDTO(UserDTO userDTO) {
		return ResponseEntitiesUtil.getUserServiceUnavailableResponseEntity();
	}

	@Override
	public ResponseEntity<List<UserDTO>> getAllusers() {
		return ResponseEntity.of(Optional.of(Arrays.asList(UserDTO.builder().id(1L).firstName("dummy firstname").lastName("dummy lastname").email("kumar.anoop@gmail.com").build())));
	}

	@Override
	public ResponseEntity<UserDTO> update(Long userId, UserDTO userDTO) {
		return ResponseEntitiesUtil.getUserServiceUnavailableResponseEntity();
	}

	@Override
	public ResponseEntity<UserDTO> getUserDTOById(Long userId) {
		return ResponseEntitiesUtil.getUserServiceUnavailableResponseEntity();
	}

	@Override
	public ResponseEntity<?> deleteUserDTO(Long userId) {
		return ResponseEntitiesUtil.getUserServiceUnavailableResponseEntity();
	}

}
