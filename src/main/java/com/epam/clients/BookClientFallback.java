package com.epam.clients;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.epam.dto.BookDTO;
import com.epam.utils.ResponseEntitiesUtil;


/**
 * @author Anoop_Dande
 *
 *Fallback for the bookServiceClient
 */
@Component
public class BookClientFallback implements BookServiceClient{

	@Override
	public ResponseEntity<BookDTO> addBook(BookDTO book) {
		return ResponseEntitiesUtil.getServiceUnavailableResponseEntity();
	}

	@Override
	public ResponseEntity<List<BookDTO>> getAllBooks() {
		return ResponseEntity.of(Optional.of(Arrays.asList(BookDTO.builder().author("dummy author").id(1L).title("dummy data").build())));
	}

	@Override
	public ResponseEntity<BookDTO> update(Long bookId, BookDTO book) {
		return ResponseEntitiesUtil.getServiceUnavailableResponseEntity();
	}

	@Override
	public ResponseEntity<BookDTO> getBookById(Long bookId) {
		return ResponseEntitiesUtil.getServiceUnavailableResponseEntity();
	}

	@Override
	public ResponseEntity<?> deleteBook(Long bookId) {
		return ResponseEntitiesUtil.getServiceUnavailableResponseEntity();
	}


}
