package com.epam.clients;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.epam.dto.UserDTO;


/**
 * @author Anoop_Dande
 *
 *Feign client for the userService
 */
@FeignClient(name = "user-service",fallback = UserClientFallback.class)
@Component
public interface UserServiceClient {

	@PostMapping("/users")
	public ResponseEntity<UserDTO> addUserDTO(@RequestBody UserDTO userDTO);

	@GetMapping("/users")
	public ResponseEntity<List<UserDTO>> getAllusers();

	@PutMapping("/users/{userId}")
	public ResponseEntity<UserDTO> update(@PathVariable Long userId, @RequestBody UserDTO userDTO);

	@GetMapping("/users/{userId}")
	public ResponseEntity<UserDTO> getUserDTOById(@PathVariable Long userId);

	@DeleteMapping("/users/{userId}")
	public ResponseEntity<?> deleteUserDTO(@PathVariable Long userId);

}
