package com.epam.constants;

/**
 * @author Anoop_Dande
 *
 *
 * Contains all the constants of the application.
 */
public class Constants {

	public static final long ACCESS_TOKEN_VALIDITY_SECONDS = (long)5 * 60 * 60;
	public static final String SIGNING_KEY = "anoop";
	public static final String TOKEN_PREFIX = "Bearer ";
	public static final String HEADER_STRING = "Authorization";

	private Constants() {
		throw new IllegalStateException("Utility class");
	}

}
