package com.epam.exceptions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Anoop_Dande
 *
 * DataNotFoundException occurs when the data is not found.
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
@SuppressWarnings("serial")
public class DataNotFoundException extends RuntimeException {

	private static final Logger logger = LogManager.getLogger(DataNotFoundException.class);

	public DataNotFoundException(String exceptionMessage) {
		super(exceptionMessage);
		logger.warn(exceptionMessage);
	}

}
