package com.epam.exceptions;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.epam.services.LibraryService;

@ResponseStatus(HttpStatus.NOT_FOUND)
@SuppressWarnings("serial")
public class BookNotFoundException extends RuntimeException {
	private static final Logger logger = LogManager.getLogger(LibraryService.class);

	/**
	 * @param exceptionMessage is message of the exception.
	 */
	public BookNotFoundException(String exceptionMessage) {

		super(exceptionMessage);
		logger.warn(exceptionMessage);

	}

}
