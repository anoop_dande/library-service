package com.epam.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.epam.clients.BookServiceClient;
import com.epam.clients.UserServiceClient;
import com.epam.dto.BookDTO;
import com.epam.dto.UserDTO;
import com.epam.dto.UserDetailsWithIssuedBooksDTO;
import com.epam.entity.IssuedBook;
import com.epam.exceptions.BookAlreadyIssuedToSomeoneException;
import com.epam.exceptions.BookNotFoundException;
import com.epam.exceptions.BookNotIssuedToAnyUser;
import com.epam.exceptions.UserNotFoundException;
import com.epam.repository.LibraryRepository;

/**
 * @author Anoop_Dande
 *
 */
@Service
public class LibraryService {
	
	private static final Logger logger = LogManager.getLogger(LibraryService.class);

	private BookServiceClient bookServiceClient;
	
	private UserServiceClient userServiceClient;
	
	private LibraryRepository libraryRepository;
	
	
	@Autowired
	public LibraryService(BookServiceClient bookServiceClient, UserServiceClient userServiceClient,
			LibraryRepository libraryRepository) {
		super();
		this.bookServiceClient = bookServiceClient;
		this.userServiceClient = userServiceClient;
		this.libraryRepository = libraryRepository;
	}

	public ResponseEntity<List<BookDTO>> getAllBooks() {
		return bookServiceClient.getAllBooks();
	}

	public ResponseEntity<BookDTO> getBook(Long bookId) {
		return bookServiceClient.getBookById(bookId);
	}

	public ResponseEntity<BookDTO> updateBook(Long bookId, BookDTO book) {
		return bookServiceClient.update(bookId, book);
	}

	public ResponseEntity<BookDTO> addBook(BookDTO book) {
		return bookServiceClient.addBook(book);
	}

	public void deleteBook(Long bookId) {
		bookServiceClient.deleteBook(bookId);
	}
	
	public ResponseEntity<List<UserDTO>> getAllUsers() {
		return userServiceClient.getAllusers();
	}

	public ResponseEntity<UserDTO> updateUser(Long userId, UserDTO userDTO) {
		return userServiceClient.update(userId, userDTO);
	}

	public ResponseEntity<UserDTO> addUser(UserDTO userDTO) {
		return userServiceClient.addUserDTO(userDTO);
	}

	public ResponseEntity<?> deleteUser(Long userId) {
		return userServiceClient.deleteUserDTO(userId);
	}
	
	
	public UserDetailsWithIssuedBooksDTO getUser(Long userId){
		UserDetailsWithIssuedBooksDTO userDetails=null;
		UserDTO userDTO=findUser(userId);
		List<BookDTO> issuedBooksToUser=getAllBooksIssuedToUser(userId);
		userDetails= new UserDetailsWithIssuedBooksDTO(userDTO,issuedBooksToUser);
		logger.info("displaying the user details of id {} with issued books",userId);
		return userDetails;
	}
	
	public UserDetailsWithIssuedBooksDTO addBookToUser(Long userId, Long bookId){
		UserDetailsWithIssuedBooksDTO userDetails=null;
		if(!isBookIssuedToAnyUser(bookId))
		{
			userDetails = proceedIssuingBookToUser(userId, bookId);
			logger.info("issued book to user successfully");
		}
		else
		{
			throw new BookAlreadyIssuedToSomeoneException("The book you are trying to add was already issued to someone");
		}
		
		return userDetails;
	}

	private UserDetailsWithIssuedBooksDTO proceedIssuingBookToUser(Long userId, Long bookId) {
		BookDTO bookDTO=findBook(bookId);
		UserDTO userDTO=findUser(userId);
		List<BookDTO> issuedBooksToUser=getAllBooksIssuedToUser(userId);
		IssuedBook issuedBook=new IssuedBook(userId, bookId);
		libraryRepository.save(issuedBook);
		issuedBooksToUser.add(bookDTO);
		return new UserDetailsWithIssuedBooksDTO(userDTO,issuedBooksToUser);
	}
	
	private List<BookDTO> getAllBooksIssuedToUser(Long userId)
	{
		List<BookDTO> issuedBookList=new ArrayList<>();
		List<Long> listOfBookIdsIssuedToUser=libraryRepository.findAllBookIds(userId);
		listOfBookIdsIssuedToUser.forEach(bookId->
		{
			BookDTO bookDTO=findBook(bookId);
			issuedBookList.add(bookDTO);
		});
		return issuedBookList;
	}
	
	private boolean isBookIssuedToAnyUser(Long bookId)
	{
		return libraryRepository.findByBookId(bookId).isPresent();
	}
	
	private UserDTO findUser(Long userId)
	{
		UserDTO userDTO=userServiceClient.getUserDTOById(userId).getBody();
		if(userDTO==null) throw new UserNotFoundException("user not found");
		logger.info("user details with userId {} found.",userId);
		return userDTO;
	}
	
	private BookDTO findBook(Long bookId)
	{
		BookDTO bookDTO=bookServiceClient.getBookById(bookId).getBody();
		if(bookDTO==null) throw new BookNotFoundException("book not found");
		logger.info("book details with bookId {} found.",bookId);
		return bookDTO;
	}
	
	public void releaseBookFromUser(Long userId, Long bookId){
		IssuedBook issuedBook=libraryRepository.findByBookId(bookId).orElseThrow(()-> new BookNotIssuedToAnyUser("book not issued to the user to release"));
		if(findUser(userId)!=null)
		{
			libraryRepository.deleteById(issuedBook.getId());
			logger.info("book has released from the user with id {}",userId);
		}
	}
	
	

}
