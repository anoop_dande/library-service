package com.epam.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.epam.entity.IssuedBook;

/**
 * @author Anoop_Dande
 *
 */
@Repository
public interface LibraryRepository extends JpaRepository<IssuedBook,Long>{
	Optional<IssuedBook> findByBookId(Long bookId);
	
	@Query(nativeQuery =true,value ="SELECT book_id FROM library_service.issued_book where user_id=?1")
	List<Long> findAllBookIds(Long userId);
}