package com.epam.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


/**
 * @author Anoop_Dande
 *
 */
@Getter
@Setter
@AllArgsConstructor
@ToString
@NoArgsConstructor
@Entity
public class IssuedBook {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(nullable = false)
	private Long userId;

	@Column(nullable = false)
	private Long bookId;
	
	public IssuedBook(Long userId, Long bookId) {
		super();
		this.userId = userId;
		this.bookId = bookId;
	}
	
}
