package com.epam.restcontrollers.v1;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.epam.dto.AuthenticationRequest;
import com.epam.dto.BookDTO;
import com.epam.dto.IssuedBookDTO;
import com.epam.dto.UserDTO;
import com.epam.dto.UserDetailsWithIssuedBooksDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Anoop_Dande
 *
 */
@RequestMapping(path="library")
@CrossOrigin(origins = "http://localhost:4200")
@Api(value = "This is a Library Resource version 1. We can use this api for making operations on books and users services as well as we can issue and release book to user.")
@ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully added"),
		@ApiResponse(code = 200, message = "The request has been succeeded "),
		@ApiResponse(code = 500, message = "Internal Server error"),
		@ApiResponse(code = 404, message = "The resource you are looking for is not found"),
		@ApiResponse(code = 400, message = "The requested resource is not allowed") })
public interface LibraryResources {

	@ApiOperation(value = "Add a new book", response = ResponseEntity.class)
	@PostMapping("/books")
	public ResponseEntity<BookDTO> addBook(@Valid @RequestBody BookDTO book);

	@ApiOperation(value = "Find all Books", response = ResponseEntity.class)
	@GetMapping("/books")
	public ResponseEntity<List<BookDTO>> getAllBooks();

	@ApiOperation(value = "Update a existing book", response = ResponseEntity.class)
	@PutMapping("/books/{bookId}")
	public ResponseEntity<BookDTO> update(@PathVariable Long bookId, @RequestBody BookDTO book);

	@ApiOperation(value = "find a Book by bookId", response = ResponseEntity.class)
	@GetMapping("/books/{bookId}")
	public ResponseEntity<BookDTO> getBookById(@PathVariable Long bookId);

	@ApiOperation(value = "Delete a Book by bookId", response = ResponseEntity.class)
	@DeleteMapping("/books/{bookId}")
	public ResponseEntity<?> deleteBook(@PathVariable Long bookId);
	
	
	@ApiOperation(value = "Add a new user", response = UserDTO.class)
	@PostMapping("/users")
	public ResponseEntity<UserDTO> addUser(@Valid @RequestBody UserDTO userDTO);

	@ApiOperation(value = "Find all Users", response = UserDTO.class, responseContainer = "List")
	@GetMapping("/users")
	public ResponseEntity<List<UserDTO>> getAllUsers();

	@ApiOperation(value = "Update a existing user", response = UserDTO.class)
	@PutMapping("users/{userId}")
	public ResponseEntity<UserDTO> update(@PathVariable Long userId, @Valid @RequestBody UserDTO user);

	@ApiOperation(value = "find a User by userId", response = UserDTO.class)
	@GetMapping("users/{userId}")
	public ResponseEntity<UserDetailsWithIssuedBooksDTO> getUser( @PathVariable Long userId);

	@ApiOperation(value = "Delete a User by userId", response = UserDTO.class)
	@DeleteMapping("users/{userId}")
	public ResponseEntity<?> deleteUser(@PathVariable Long userId);
	
	@ApiOperation(value = "Add a book to user", response = IssuedBookDTO.class)
	@PostMapping("users/{user_id}/books/{book_id}")
	public ResponseEntity<UserDetailsWithIssuedBooksDTO> issueBookToUser(@PathVariable("user_id") Long userId,@PathVariable("book_id") Long bookId);

	@ApiOperation(value = "Remove book which was issued to user", response = IssuedBookDTO.class)
	@DeleteMapping("users/{user_id}/books/{book_id}")
	public ResponseEntity<?> releaseBookFromUser(@PathVariable("user_id") Long userId,@PathVariable("book_id") Long bookId);

//	@ApiOperation(value = "Authenticate the user")
//	@PostMapping("authenticate")
//	public ResponseEntity<Object> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest);

}
