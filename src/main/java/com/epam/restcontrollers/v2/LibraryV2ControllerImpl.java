package com.epam.restcontrollers.v2;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.epam.dto.BookDTO;
import com.epam.dto.UserDTO;
import com.epam.dto.UserDetailsWithIssuedBooksDTO;
import com.epam.services.LibraryService;

/**
 * @author Anoop_Dande
 *
 */
@RestController
public class LibraryV2ControllerImpl implements LibraryV2Resources {

	@Autowired
	LibraryService libraryService;

	@Override
	public ResponseEntity<BookDTO> addBook(BookDTO book) {
		return libraryService.addBook(book);
	}

	@Override
	public ResponseEntity<List<BookDTO>> getAllBooks() {
		return libraryService.getAllBooks();
	}

	@Override
	public ResponseEntity<BookDTO> update(Long bookId, BookDTO book) {
		return libraryService.updateBook(bookId, book);
	}

	@Override
	public ResponseEntity<BookDTO> getBookById(Long bookId) {
		return libraryService.getBook(bookId);
	}

	@Override
	public ResponseEntity<?> deleteBook(Long bookId) {
		libraryService.deleteBook(bookId);
		return ResponseEntity.ok().build();
	}

	@Override
	public ResponseEntity<UserDTO> addUser(@Valid UserDTO userDTO) {
		return libraryService.addUser(userDTO);
	}

	@Override
	public ResponseEntity<List<UserDTO>> getAllUsers() {
		return libraryService.getAllUsers();
	}

	@Override
	public ResponseEntity<UserDTO> update(Long userId, @Valid UserDTO userDTO) {
		return libraryService.updateUser(userId, userDTO);
	}

	@Override
	public ResponseEntity<UserDetailsWithIssuedBooksDTO> getUser(Long userId) {
		return ResponseEntity.ok(libraryService.getUser(userId));
	}

	@Override
	public ResponseEntity<?> deleteUser(Long userId) {
		return libraryService.deleteUser(userId);
	}

	@Override
	public ResponseEntity<UserDetailsWithIssuedBooksDTO> issueBookToUser(Long userId, Long bookId) {
		return ResponseEntity.ok(libraryService.addBookToUser(userId, bookId));
	}

	@Override
	public ResponseEntity<?> releaseBookFromUser(Long userId, Long bookId) {
		 libraryService.releaseBookFromUser(userId, bookId);
		 return ResponseEntity.noContent().build();
	}

}
