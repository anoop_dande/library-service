package com.epam.dto;

import io.swagger.annotations.ApiModelProperty;


/**
 * @author Anoop_Dande
 *
 */
public class IssuedBookDTO {
		@ApiModelProperty(notes = "The database auto generated id", required = true)
		private Long id;

		@ApiModelProperty(notes = "The user id who have taken the specific book", required = true)
		private Long userId;

		@ApiModelProperty(notes = "The book id which was alloted to the user", required = true)
		private Long bookId;
}
