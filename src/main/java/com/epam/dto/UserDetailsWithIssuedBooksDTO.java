package com.epam.dto;

import java.util.List;

import org.springframework.http.ResponseEntity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


/**
 * @author Anoop_Dande
 *
 */
@Getter
@Setter
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Builder
public class UserDetailsWithIssuedBooksDTO {
	
	@ApiModelProperty(notes = "The details of user", required = true)
	private UserDTO userDTO;
	
	@ApiModelProperty(notes = "The list of books issued to user", required = true)
	List<BookDTO> issuedBooksToUser;

}
