package com.epam.dto;


import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Builder
public class UserDTO {

	@ApiModelProperty(notes = "The database auto generated User id", required = true)
	private Long id;

	@ApiModelProperty(notes = "The first name must contain atleast 2 and maximum 100 characters", required = true)
	private String firstName;

	@ApiModelProperty(notes = "The last name must contain atleast 2 and maximum 100 characters", required = true)
	private String lastName;

	@ApiModelProperty(notes = "The email must be unique and contain atleast 2 and maximum 100 characters", required = true)
	private String email;
}
