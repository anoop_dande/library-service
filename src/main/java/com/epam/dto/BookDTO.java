package com.epam.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


/**
 * @author Anoop_Dande
 *
 */
@Getter
@Setter
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Builder
public class BookDTO {

	@ApiModelProperty(notes = "The database auto generated Book id", required = true)
	private Long id;

	@ApiModelProperty(notes = "The title must contain atleast 2 and maximum 100 characters", required = true)
	private String title;

	@ApiModelProperty(notes = "The author must contain atleast 2 and maximum 100 characters", required = true)
	private String author;

}