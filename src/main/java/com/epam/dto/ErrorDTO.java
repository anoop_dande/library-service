package com.epam.dto;

import org.springframework.http.HttpStatus;

public class ErrorDTO {
	private HttpStatus statusOfError;
	private String errorMessage;

	/**
	 * @param statusOfError
	 * @param errorMessage
	 */
	public ErrorDTO(HttpStatus statusOfError, String errorMessage) {
		super();
		this.statusOfError = statusOfError;
		this.errorMessage = errorMessage;
	}

	public HttpStatus getStatusOfError() {
		return statusOfError;
	}

	public void setStatusOfError(HttpStatus statusOfError) {
		this.statusOfError = statusOfError;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
