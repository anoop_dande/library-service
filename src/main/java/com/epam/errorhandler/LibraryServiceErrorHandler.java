package com.epam.errorhandler;



import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.epam.dto.ErrorDTO;
import com.epam.exceptions.DataNotFoundException;
import com.epam.exceptions.UserNotFoundException;

import feign.FeignException;

/**
 * @author Anoop_Dande
 *
 */
@RestControllerAdvice
public class LibraryServiceErrorHandler {

	/**
	 * @param exception
	 * @returns the handled exception message.
	 */
	@ExceptionHandler({ UserNotFoundException.class })
	public ResponseEntity<Object> handleUserNotFoundException(UserNotFoundException exception) {
		ErrorDTO error = new ErrorDTO(HttpStatus.NOT_FOUND, exception.getMessage());
		return new ResponseEntity<>(error, new HttpHeaders(), error.getStatusOfError());
	}

	@ExceptionHandler({ DataNotFoundException.class })
	public ResponseEntity<Object> handleDataNotFoundException(DataNotFoundException exception) {
		ErrorDTO error = new ErrorDTO(HttpStatus.NOT_FOUND, exception.getMessage());
		return new ResponseEntity<>(error, new HttpHeaders(), error.getStatusOfError());
	}

	
	@ExceptionHandler({ FeignException.class })
	public ResponseEntity<Object> handleFeignStatusException(FeignException exception) {
		ErrorDTO error = new ErrorDTO(HttpStatus.NOT_FOUND, exception.getMessage());
		return new ResponseEntity<>(error, new HttpHeaders(), error.getStatusOfError());
	}

}
