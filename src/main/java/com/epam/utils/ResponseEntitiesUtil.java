package com.epam.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.epam.dto.BookDTO;
import com.epam.dto.UserDTO;

public class ResponseEntitiesUtil {
    
    public static ResponseEntity<BookDTO> getServiceUnavailableResponseEntity(){
        return new ResponseEntity<BookDTO>(HttpStatus.SERVICE_UNAVAILABLE);
    }
    
    public static ResponseEntity<UserDTO> getUserServiceUnavailableResponseEntity(){
        return new ResponseEntity<UserDTO>(HttpStatus.SERVICE_UNAVAILABLE);
    }
 
}